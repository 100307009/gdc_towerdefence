extends Node

var tower_data = {
	"GunT1": {
		"damage": 20,
		"rof": 1,
		"range": 350,
		"category": "Projectile"
	},
	"MissileT1": {
		"damage": 100,
		"rof": 3,
		"range": 550,
		"category": "Missile"
	},
	"GunT2": {
		"damage": 40,
		"rof": 1,
		"range": 400,
		"category": "Projectile"
	}
}
